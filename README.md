# How's my room Telegram bot
---
Bot that monitors the temperature of a room. This is a new version of the project, in which the DHT11 sensor was replaced 
by Adafruit **[MCP9808 sensor](https://learn.adafruit.com/adafruit-mcp9808-precision-i2c-temperature-sensor-guide/overview)** because
is more accurate.

I am using **Telegram** as messaging service and **Python** as a programming language, and
 **Docker** containers.
  
Here you can find all the required files.

---

## Hardware
### Raspberry pi 3 Model B Rev 1.2
The raspberry pi 3 is a small single-board computer, Quad Core 1.2GHz Broadcom BCM2837 64bit CPU, with 40-pin extended GPIO.

### MCP9808
The MCP9808 is a I2C sensor. "**[I2C](https://en.wikipedia.org/wiki/I%C2%B2C)** (I-squared-C) is a  synchronous, multi-master, 
multi-slave, packet switched, single-ended, serial communication bus."

### Wiring
Wiring is quite straightforward:
 
![howsmyroom_bb](images/howsmyroom_bb.png) 
 
 * Connect MCP9808 GND to Pi GND (GPIO 5)
 * Connect MPC9808 Vcc to Pi Vcc (GPIO 1)
 * Connect MPC9808 SDA to Pi SDA (GPIO 2)
 * Connect MPC9808 SCL to Pi SCL (GPIO 3)

---

## Code

### Docker

* To run the container in the background:
  
  $docker run -d --privileged mcp9808 python roomtemp_bot.py
  
  or 
  
  $docker run --device /dev/gpiomem -d mcp9808 python roomtemp_bot.py

---
##  Telegram bot

To accesss to the bot, find the **@roomtemp_bot** username in Telegram.

### Commands
 
The commands you will find:
 
#### **/start** 
As all bots, the user should start the conversation. So type /start. A message is displayed with the following information:

![start_tempbot](images/start_tempbot.png) 
    
#### /help for more information

![help_tempbot](images/help_tempbot.png)  


#### /temp (c or f) 
You will get the room temperature °C or F degrees
![temp_tempbot](images/temp_tempbot.png)

#### /set (seconds)
To determine how often you want to receive the temperature information
![set_tempbot](images/set_tempbot.png) 

#### Unset timer /unset
![unset_tempbot](images/unset_tempbot.png) 

#### /monitor_timer 
To set a 
<start time (dd-mm-yyyy-00:00 24 hrs)> <end time (dd-mm-yyyy-00:00 24 hrs)> <sampling time (s)>
![timer_tempbot](images/timer_tempbot.png)

    




