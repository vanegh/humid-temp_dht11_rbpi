import getToken
import mcp9808
import timer_async
import logging
import plot

from datetime import datetime
from telegram.ext import Updater, CommandHandler, MessageHandler, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

# Helping functions
def format_datetime(datetime_args):
    date=datetime_args.split('-')[:3] #array [dd,mm,yyyy]
    hourMin=datetime_args.split('-')[-1] #hh:mm
    hour=hourMin.split(':')[0]
    minutes=hourMin.split(':')[1]
    return datetime(int(date[2]),int(date[1]),int(date[0]),int(hour),int(minutes),0)


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

def start(bot,update):
    """Send a message when the command /start is issued."""
    button_list=[
        [InlineKeyboardButton("Temperature in °C",callback_data='TC'),
        InlineKeyboardButton("Temperature in F",callback_data='TF')]    
        ]
    reply_markup=InlineKeyboardMarkup(button_list)
    start_text="Hello I am a room monitoring bot. " \
               "I can tell you what is the temperature in your room. \n"\
               "Type /help for more information."
               
    
    update.message.reply_text(start_text,reply_markup=reply_markup)

def check_conditions(bot, update):
    query=update.callback_query
    print(query.data)
    if query.data == 'TC':
        txt=mcp9808.check_temp()
    elif query.data =='TF':
        txt=mcp9808.check_temp(units="F")

    
    bot.edit_message_text(text=txt,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id)


def temp(bot, update, args):
    print(args, len(args))
    try:
        if args[0] == "F" or  args[0]== "f":
            txt=mcp9808.check_temp(units="F")
        elif args[0] == "C" or args[0] == "c":
            txt=mcp9808.check_temp()
        else:
            txt="Incorrect value. Type /temp C or F."
    
    except (IndexError, ValueError) :
        txt= "None or incorrect value. Type /temp C or F."
        
    bot.send_message(chat_id=update.message.chat_id, text=txt)

def alarm(bot, job):
    bot.send_message(job.context, text=mcp9808.check_temp())

def set_timer(bot, update, args, job_queue, chat_data):
    """Add a job  to the queue. """
    chat_id=update.message.chat_id
    try:
        due=int(args[0])
        if due < 0:
            update.message.reply_text('You should give positive numbers')
            return
        job= job_queue.run_repeating(alarm, due, context=chat_id)
        chat_data['job']=job
        update.message.reply_text('Timer successfully set!')

    except (IndexError, ValueError):
        update.message.reply_text('Usage: /set <seconds>')



def unset_timer(bot, update, chat_data):
    """Remove the job """
    if 'job' not in chat_data:
        update.message.reply_text('You have no active timer')
        return
    job=chat_data['job']
    job.schedule_removal()
    del chat_data['job']
    update.message.reply_text('Timer succesfully unset!')



def monitor_timer(bot,update,args):
    print (args)
    try:  
        start=format_datetime(args[0])
        end=format_datetime(args[1])
        sampling=int(args[2])
        print(start,end,sampling)
        timer_async.timer(start,end,sampling)
        txt="Monitor Timer collected data successfully."
        plot.plot_data(start,end)
        bot.send_photo(chat_id=update.message.chat_id, photo=open('tempe.png', 'rb'))
    except IndexError:
        txt='None or few arguments. Type /monitor_timer <start timer dd-mm-yyyy-hh:mm 24 hrs>\n'\
        "<end timer dd-mm-yyyy-hh:mm 24 hrs > <sampling time (s)>"
        print(txt)
    update.message.reply_text(txt)           


def help(bot, update):
    help_text='You can check Temperature in °C or F. \n'\
            '/temp <F or C>. Temperature in Celsius (C) or Farenheit (F) \n'\
            '/set <seconds> to determine how often you want to receive the status of the room conditions \n'\
            'Unset timer /unset \n'\
            'Set timer /monitor_timer <start time (dd-mm-yyyy-00:00 24 hrs)> <end time (dd-mm-yyyy-00:00 24 hrs)> <sampling time (s)> for data storage in a database'\
            'Set timer /set <seconds> to determine how often you want to monitor the room conditions- \n'\
            'Unset timer /unset \n'\


    update.message.reply_text(help_text)


def main():
    # Create the Updater and pass it bot's token.
    updater = Updater(token=getToken.token)
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help",help))
    dispatcher.add_handler(CommandHandler("temp",temp, pass_args=True))
    dispatcher.add_handler(CallbackQueryHandler(check_conditions))
    dispatcher.add_handler(CommandHandler("set",set_timer,
                                          pass_args=True,
                                          pass_job_queue=True,
                                          pass_chat_data=True))
    dispatcher.add_handler(CommandHandler("unset", unset_timer, pass_chat_data=True))

    dispatcher.add_handler(CommandHandler("monitor_timer",monitor_timer, pass_args=True))

    # Start the Bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
