import time
import board
import busio
import adafruit_mcp9808

i2c_bus=busio.I2C(board.SCL, board.SDA)
mcp=adafruit_mcp9808.MCP9808(i2c_bus)

def check_temp(var=None,units="C"):
    if units=="C" and var==None:
        tempC=mcp.temperature
        return 'Temperature {} °C'.format(tempC)
    elif units == "F" and var == None: 
        tempF=(mcp.temperature)*9/5 +32
        return 'Temperature {} F'.format(tempF)
    elif var=="varOnly":
        return mcp.temperature
    else:
        return 'Undefined option. Type /temp <F or C>'
            

