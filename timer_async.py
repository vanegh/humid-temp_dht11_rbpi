import asyncio
import datetime
import time
import mcp9808
import mongodb_capture
import getMonDB
import configparser

def timer(start, end, sampling):

    def timer_data(end_time, loop):
        if loop.time() < end_time:
            now=datetime.datetime.now()
            
            #print(now)
        
            temp=mcp9808.check_temp(var="varOnly")
            datab.insert_data(temp,now)
            print(temp,now)
            loop.call_later(sampling, timer_data, end_time, loop)
        else:
            loop.stop()


    #loop=asyncio.get_event_loop()
    loop=asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop=asyncio.get_event_loop()
    start_time=start.timestamp()
    end_time=end.timestamp()
    execution_time_loop=start_time-(time.time()-loop.time()) #convert to local clock time
    delta_time=end_time-start_time
    end_time= execution_time_loop + delta_time

    loop.call_at(execution_time_loop, timer_data, end_time, loop)
    loop.run_forever()
    loop.close()



#start=datetime.datetime(2019,1,26,15,16,0)
#end=datetime.datetime(2019,1,26,15,22,0)
#sampling=60

user=getMonDB.user
password=getMonDB.password
cluster=getMonDB.cluster
database=getMonDB.database
collection=getMonDB.collection
datab=mongodb_capture.MongoDB(user,password,cluster,database,collection)

#timer(start,end,sampling)

