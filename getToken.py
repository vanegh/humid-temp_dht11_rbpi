import configparser

# Get info
config = configparser.ConfigParser()
config.read('config.ini')
# Get Token from config.ini file
token = config['telegram.org']['Token']
