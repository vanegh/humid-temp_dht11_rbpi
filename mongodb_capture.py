from pymongo import MongoClient, errors

import configparser
import datetime

class MongoDB:
    def __init__(self, user, password, cluster, db, collection):
        self.user= user
        self.password= password
        self.cluster=cluster
        self.db=db
        self.collectionName= collection
        self._client=self.__connect()
        self._collection=self.__get_collection()

    def __connect(self):
        try:
            return MongoClient("mongodb+srv://{}:{}@{}/".format(self.user, self.password, self.cluster))
        except errors.ConnectionFailure as e:
            return "Could not connect to server: %s" % e
        except errors.ConfigurationError as e:
            return "Could not connect to server: %s" % e
        except errors.ServerSelectionTimeoutError as e:
            return "Could not connect to server: %s" % e

    def __get_collection(self):
        if self._client is not None:
            db=self._client[self.db]
            return db[self.collectionName]

    def insert_data(self, temp,time):
        if self._collection is not None:
            data={"temperature":temp,
                  "last_modified":time}
            self._collection.insert_one(data)
    def get_data(self, start, end):
        if self._collection is not None:
            query={"last_modified": {"$gte":start, "$lte": end}}
            projection={"_id":0}
            return list(self._collection.find(query,projection))
        

           
