import getMonDB
import mongodb_capture
import datetime
import matplotlib
import matplotlib.pyplot as plt


user=getMonDB.user
password=getMonDB.password
cluster=getMonDB.cluster
database=getMonDB.database
collection=getMonDB.collection

def plot_data(start, end):
    
    datab=mongodb_capture.MongoDB(user,password,cluster,database,collection)
    #start=datetime.datetime(2019,2,14,7,0,0,0)
    #end=datetime.datetime(2019,2,14,10,0,0,0)
    collect=datab.get_data(start, end)
    temp=[variable['temperature'] for variable in collect]
    time=[variable['last_modified'] for variable in collect]
    #print(temp,time)

    plt.clf()
    fig, ax = plt.subplots()
    ax.plot(time, temp)
    ax.set(xlabel='time (min)', ylabel='Temperature (°C)',
       title="Temperature °C vs Time in minutes")
    ax.grid()
    fig.savefig('tempe.png')
    #plt.show()

