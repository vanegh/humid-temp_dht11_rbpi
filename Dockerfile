FROM balenalib/raspberrypi3-debian-python:3.5-stretch
ENTRYPOINT []
WORKDIR /roomtemp_telegram
ADD . /roomtemp_telegram
RUN apt-get update &&  apt-get install build-essential && apt-get install pkg-config \
    && apt-get install libffi-dev && apt-get install libfreetype6-dev && apt-get install libssl-dev 
RUN pip install --upgrade pip &&  pip install -r requirements.txt 
RUN apt-get -y install tzdata && ln -sf /usr/share/zoneinfo/America/Mexico_City /etc/localtime 
CMD ["python", "roomtemp_bot.py"]


