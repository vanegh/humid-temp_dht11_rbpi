import configparser

config=configparser.ConfigParser()

config.read('config.ini')

user=config['mongoAtlas']['user']
password=config['mongoAtlas']['password']
cluster=config['mongoAtlas']['cluster']
database=config['mongoAtlas']['database']
collection=config['mongoAtlas']['collection']
